# dicehub

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A single-threaded NMDC server using non-blocking winsock.

"This is the beginnings of an(other) NMDC server. It's single-threaded C++ and seems to work. It might be interesting to use as a basis for some other hobby project, since the source code is short."

Tags: nmdc

## See Also

Original thread: http://forums.apexdc.net/topic/3985-dicehub/


## Download

- [⬇️ dicehub-0.2.rar](dist-archive/dicehub-0.2.rar) *(15.06 KiB)*
- [⬇️ dicehub-0.1.rar](dist-archive/dicehub-0.1.rar) *(14.78 KiB)*
